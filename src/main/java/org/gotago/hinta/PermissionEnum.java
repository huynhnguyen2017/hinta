package org.gotago.hinta;

import java.util.stream.Stream;

public enum PermissionEnum {
    ANY_ONE("ANY_ONE"), ONLY_ME("ONLY_ME");

    private String value;

    PermissionEnum(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public static Stream<PermissionEnum> stream() {
        return Stream.of(PermissionEnum.values());
    }
}
