package org.gotago.hinta;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HintaApplication {

	public static void main(String[] args) {
		SpringApplication.run(HintaApplication.class, args);
	}

}
