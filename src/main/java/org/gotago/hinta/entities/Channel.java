package org.gotago.hinta.entities;

import org.gotago.hinta.PermissionEnum;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import java.util.List;

@Entity
public class Channel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String channelName;

    private String pinCode;

    private String coverImagePath;

    @Enumerated(EnumType.STRING)
    private PermissionEnum permission;

    @OneToMany(mappedBy = "channel")
    private List<Question> questions;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "detailChannel", orphanRemoval = true)
    private List<UserChannelDetail> userChannels;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getChannelName() {
        return channelName;
    }

    public void setChannelName(String channelName) {
        this.channelName = channelName;
    }

    public String getPinCode() {
        return pinCode;
    }

    public void setPinCode(String pinCode) {
        this.pinCode = pinCode;
    }

    public String getCoverImagePath() {
        return coverImagePath;
    }

    public void setCoverImagePath(String coverImagePath) {
        this.coverImagePath = coverImagePath;
    }

    public PermissionEnum getPermission() {
        return permission;
    }

    public void setPermission(PermissionEnum permission) {
        this.permission = permission;
    }

    public List<Question> getQuestions() {
        return questions;
    }

    public void setQuestions(List<Question> questions) {
        this.questions = questions;
    }

    public List<UserChannelDetail> getUserChannels() {
        return userChannels;
    }

    public void setUserChannels(List<UserChannelDetail> userChannels) {
        this.userChannels = userChannels;
    }
}
