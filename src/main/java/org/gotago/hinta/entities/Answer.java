package org.gotago.hinta.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;

@Entity
public class Answer {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String answerString;

    private Long questionId;

    @OneToOne(mappedBy = "correctAnswer")
    private Question questionForCorrectAnswer;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAnswerString() {
        return answerString;
    }

    public void setAnswerString(String answerString) {
        this.answerString = answerString;
    }

    public Long getQuestionId() {
        return questionId;
    }

    public void setQuestionId(Long questionId) {
        this.questionId = questionId;
    }

    public Question getQuestionForCorrectAnswer() {
        return questionForCorrectAnswer;
    }

    public void setQuestionForCorrectAnswer(Question questionForCorrectAnswer) {
        this.questionForCorrectAnswer = questionForCorrectAnswer;
    }
}
