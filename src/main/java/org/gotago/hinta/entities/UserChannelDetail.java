package org.gotago.hinta.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import java.time.LocalDateTime;

@Entity
public class UserChannelDetail {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "channel_id", referencedColumnName = "id")
    private Channel detailChannel;

    @ManyToOne
    @JoinColumn(name = "user_id", referencedColumnName = "id")
    private User detailUser;

    private LocalDateTime createdAt;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Channel getDetailChannel() {
        return detailChannel;
    }

    public void setDetailChannel(Channel detailChannel) {
        this.detailChannel = detailChannel;
    }

    public User getDetailUser() {
        return detailUser;
    }

    public void setDetailUser(User detailUser) {
        this.detailUser = detailUser;
    }

    public LocalDateTime getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(LocalDateTime createdAt) {
        this.createdAt = createdAt;
    }
}
